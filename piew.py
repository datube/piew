#!/usr/bin/env python

# Copyright (C) 2018 datube
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this package; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#
#    On Debian systems, the complete text of the GNU General
#    Public License can be found in `/usr/share/common-licenses/GPL-3'.


# import required modules  ---------------------------------------------------
import os               # Miscellaneous operating system interfaces
import sys              # System-specific parameters and functions
if sys.version_info > (3, 0):
  from tkinter import *   # Python's de-facto standard GUI package
else:
  from Tkinter import *   # Python's de-facto standard GUI package
from PIL import ImageTk # Python Imaging Library
from PIL import Image   # Python Imaging Library
import argparse         # Parser for command-line options
import random           # Generate pseudo-random numbers
import datetime         # Basic date and time types


# Classes ====================================================================
class Config():
  # key definitions; not really pretty, but okay for now
  BACKTICK  = 49
  BCKSPC    = 22
  BRACKETC  = 35
  BRACKETO  = 34
  END       = 115
  EQUALS    = 21
  HOME      = 110
  KEYB      = 56
  KEYF      = 41
  KEYH      = 43
  KEYJ      = 44
  KEYK      = 45
  KEYL      = 46
  KEYN      = 57
  KEYO      = 32
  KEYP      = 33
  KEYQ      = 24
  KEYR      = 27
  KEYS      = 39
  MINUS     = 20
  PGDN      = 117
  PGUP      = 112
  SPACE     = 65

  def __init__(self):
    self.ext = 'jpg|png|jpeg|bmp|gif'
    self.files = [-1, -1 , [], [], []]
    self.arg_file  = None
    # fix self.ext to make it more usable
    ext = self.ext
    self.ext = [".{}".format(e.lower()) for e in ext.split('|') ]
    self.ext.extend([".{}".format(e.upper()) for e in ext.split('|') ])
    self.tkto = None

    self.opt_rand = False
    self.opt_wait = 3000
    self.opt_auto = False
    self.opt_lock = False
    self.opt_lock_files = None
    self.opt_orig = False
    self.opt_recursive = True
    self.opt_slideshow = False

class PUI(): # Tkinter GUI for piew ------------------------------------------
  # default method handler (pass unknown onto self.root)
  def __getattr__(self,name):
    def handler(*args):
      return getattr(self.root, name)(*args)
    return handler

  def __init__(self, root):
    # keep track of fore/back-ground color and used font
    self.bgcolor = 'black'
    self.fgcolor = 'lightgray'
    self.font = 'Monospace 8'
    self.maxlen = 50
    # keep track of root Tk object and change appearance
    self.root = root
    self.root.configure(bg=self.bgcolor)
    # keep a reference to the loaded Image
    self.img_copy = None
    # keep track of width/height
    self.width = None
    self.height = None
    # zoom / panning
    self.zoom = 1.0
    self.zoom_step = 0.2
    self.panx = 0
    self.pany = 0
    # fullscreen
    self.fullscreen = False
    # create a new canvas object without a border
    self.image = Canvas(self.root, bg=self.bgcolor, highlightthickness=0)
    self.image.pack(fill=BOTH, expand=YES)
    # where to put it on the window
    self.image.place(relwidth=1, relheight=1)
    # create a new informational label
    ## a new object to dynamically set the label text
    str_info = StringVar()
    str_info.set(sys.argv[0])
    self.info = Label(self.root, textvariable = str_info,
        bg=self.bgcolor, fg=self.fgcolor, font=self.font,
        highlightthickness=1, highlightcolor=self.fgcolor,
        padx=8, pady=5, justify=LEFT )
    self.info.text = str_info
    self.info.pack()
    self.info.place(bordermode=INSIDE, anchor = NW, x = 15, y = 15)
    self.info.lower()
    self.info_visible = False
    # resize event binding
    self.image.bind("<Configure>", self._configure)

  def reset_zoom(self): #-----------------------------------
    self.zoom = 1.0
    self.panx = 0
    self.pany = 0

  def _configure(self, event): #--------------------------
    self.width = event.width
    self.height = event.height
    # enlarge info font depending on width
    _fs = 8
    if self.width > 1280:
      _fs = int(_fs*(self.width/1280.0))
    self.info.config(font="{} {}".format(self.font.rsplit(" ",1)[0], _fs))
    # redraw_image will do nothing if there is no "image"
    self.redraw_image()
    self.update_info()

  def toggle_info(self): #---------------------------------
    self.info_visible = not self.info_visible
    self.update_info()
    getattr(self.info, ('lift' if self.info_visible else 'lower'))()

  def update_info(self): #-----------------------------
    self.update_title()
    if self.info_visible:
      st = os.stat(self.filename)
      mtime = datetime.datetime.fromtimestamp(st.st_mtime
                ).strftime('%Y/%m/%d %H:%M:%S')
      path, name = self.filename.rsplit('/',1)
      # path, name maxlen
      path = (path[self.maxlen:] and '...') + path[-self.maxlen:]
      name = (name[self.maxlen:] and '...') + name[-self.maxlen:]

      info = "path  : {}\n"+\
             "file  : {}\n"+\
             "date  : {}\n"+\
             "image : {}{} {}\n"+\
             "size  : {}\n"+\
             "\nfiles : {} {}{}"
      info = info.format( path,
                          name,
                          mtime,
                          "{}x{}".format(self.img_width, self.img_height),
                          ('' if c.opt_orig else
                            ' ({}x{})'.format(self.img_new_width,
                              self.img_new_height)),
                          ('' if self.zoom == 1 else '/ {}x'.format(self.zoom)),
                          self._hsize(st.st_size),
                          # files...
                          (str(len(c.files[2]))+(
                            ("/"+str(len(c.opt_lock_files[2])))
                              if c.opt_lock else '')),
                            ('/ random ' if c.opt_rand else ''),
                            ("/ auto ({:0.1f}s) ".format(
                              c.opt_wait/1000.0) if c.opt_auto else ''))
      self.info.text.set(info)

  def _hsize(self, size, precision=0): #----------
    #http://code.activestate.com/recipes/577081-humanized-representation-of-a-number-of-bytes
    suffixes=['B','KB','MB','GB','TB']
    suffixIndex = 0
    while size > 1024:
      suffixIndex += 1 #increment the index of the suffix
      size = size/1024.0 #apply the division
    return "{:.0f} {}".format(size,suffixes[suffixIndex])

  def loadimage(self, filename): #-------------------------
    self.filename = filename
    self.reset_zoom()
    self.img_width = self.img_height = 0
    self.img_new_height = self.img_new_width = 0
    if os.path.exists(filename):
      try:
        self.img_copy = Image.open(filename).copy()
      except IOError:
        print("WARN: unable to load `{}'".format(filename))
        pass
      else:
        self.redraw_image()
    self.update_info()

  def redraw_image(self): #--------------------------------
    if self.img_copy and self.width:
      image = self.img_copy
      # image dimensions
      img_width, img_height = image.size
      # keep track of width/height
      self.img_width = img_width
      self.img_height = img_height
      # tk object dimensions
      tko_width = self.width
      tko_height = self.height
      # set newimage dimensions
      new_width = tko_width
      new_height = tko_height
      if c.opt_orig:
        # skip resize
        new_height= img_height
        new_width = img_width
        self.img_new_height = img_height
        self.img_new_width = img_width
      else:
        # calculate image ratio
        img_ratio = float(img_height) / img_width
        # image scaling with aspect of ratio
        if img_width > img_height:
          new_height = tko_width * img_ratio
          if new_height > tko_height:
            new_width = tko_height / img_ratio
        else:
          new_width = tko_height / img_ratio
          if new_width > tko_width:
            new_width = tko_width
        new_height = new_width * img_ratio
      # apply zoom factor
      if self.zoom<self.zoom_step:
        self.zoom = self.zoom_step
      new_width  *= self.zoom
      new_height *= self.zoom
      # keep track of new_width/new_height
      self.img_new_width = int(new_width)
      self.img_new_height = int(new_height)
      # check/fix allowed pan values
      pany_max = (new_height - tko_height)/2
      if pany_max > 0:
        if abs(self.pany) > pany_max:
          self.pany = pany_max * (-1 if self.pany<0 else 1)
      else:
        self.pany = 0
      panx_max = (new_width - tko_width)/2
      if panx_max > 0:
        if abs(self.panx) > panx_max:
          self.panx = panx_max * (-1 if self.panx<0 else 1)
      else:
        self.panx = 0
      # resize the image
      image = image.resize((int(new_width), int(new_height)), Image.ANTIALIAS)
      # load the image
      self.imagetk = ImageTk.PhotoImage(image)
      # put it on the canvas centered
      self.image.create_image(
          self.panx + int(tko_width / 2), self.pany + int(tko_height / 2),
          anchor=CENTER, image = self.imagetk )

  def update_title(self, title=None): #--------------------
    # automatically add some settings info
    if not title:
      title=((self.filename[self.maxlen:] and '...')+self.filename[-self.maxlen:])
    s = '{}{}{}{}'.format(
        ( 'o' if c.opt_orig else '' ),
        ( 'p' if c.opt_lock else '' ),
        ( 'r' if c.opt_rand else '' ),
        ( 's' if c.opt_auto else '' ),
        )
    s = ( ' [{}]'.format(s) if s else '')
    self.root.title(title+s)


  def toggle_fullscreen(self): #---------------------------
    self.fullscreen = not self.fullscreen
    self.root.attributes("-fullscreen", self.fullscreen)


# Callback functions =========================================================
def cb_root_keypress(event): #------------------------------------------------
  keycode = event.keycode

  if keycode == c.KEYQ:
    tkgui.attributes("-fullscreen", False)
    tkgui.destroy()
  elif event.keycode == c.KEYP:
    keypress_pathlock()
  elif keycode == c.BACKTICK:
    tkgui.toggle_info()
  elif keycode in [c.BRACKETO, c.BRACKETC]:
    keypress_interval(keycode)
  elif keycode in [c.KEYH, c.KEYJ, c.KEYK, c.KEYL]:
    keypress_panning(keycode)
  elif keycode in [c.MINUS, c.EQUALS]:
    keypress_zoom(keycode)
  elif keycode in [c.PGUP, c.PGDN, c.BCKSPC, c.SPACE, c.KEYB, c.KEYN,
                   c.HOME, c.END]:
    keypress_navigate(keycode)
  elif keycode == c.KEYR:
    keypress_random()
  elif keycode == c.KEYF:
    tkgui.toggle_fullscreen()
  elif keycode == c.KEYS:
    keypress_auto()
  elif keycode in [c.KEYO]:
    keypress_origsize()
  #else:
  #  print("keypress: char={}, keycode={}".format(event.char, event.keycode))


def cb_root_mouse(event): #---------------------------------------------------
  # https://www.daniweb.com/programming/software-development/code/217059/using-the-mouse-wheel-with-tkinter-python
  # respond to wheel event
  if event.num == 5 or event.delta == -120:
    keypress_navigate(c.PGDN)
  if event.num == 4 or event.delta == 120:
    keypress_navigate(c.PGUP)


def cb_root_timer(): #--------------------------------------------------------
  c.tkto = None
  # simulate pgdn keypress
  keypress_navigate(c.PGDN)


def cb_start_slideshow(): #---------------------------------------------------
  tkgui.toggle_fullscreen()
  keypress_navigate(c.PGDN)
  keypress_auto()

# keypress handlers ==========================================================
def keypress_panning(keycode): #----------------------------------------------
  if keycode == c.KEYH or keycode == c.KEYL:
    tkgui.panx += tkgui.zoom * (-20 if keycode == c.KEYL else 20)
  if keycode == c.KEYJ or keycode == c.KEYK:
    tkgui.pany += tkgui.zoom * (-20 if keycode == c.KEYJ else 20)
  tkgui.redraw_image()


def keypress_zoom(keycode): #-------------------------------------------------
  tkgui.zoom += (-.2 if keycode == c.MINUS else .2)
  tkgui.redraw_image()
  tkgui.update_info()


def keypress_navigate(keycode): #---------------------------------------------
  if c.tkto: # cancel timer, if any
    tkgui.after_cancel(c.tkto)
    c.tkto = None
  idx = c.files[c.opt_rand*1]
  if keycode in [c.PGUP, c.BCKSPC, c.KEYB]:
    idx -= 1
    if idx < 0:
      idx = len(c.files[2])-1
  elif keycode in [c.PGDN, c.SPACE, c.KEYN]:
    idx += 1
    if idx > len(c.files[2])-1:
      idx = 0
  elif keycode in [c.HOME, c.END]:
    idx = (0 if keycode == c.HOME else len(c.files[2])-1)
  c.files[c.opt_rand*1] = idx
  tkgui.loadimage(c.files[2][c.files[3+c.opt_rand*1][idx]])
  if c.opt_auto: # start timer
    c.tkto = tkgui.after(c.opt_wait, cb_root_timer)


def keypress_random(): #------------------------------------------------------
  c.opt_rand = not c.opt_rand
  if not c.opt_rand:
    # continue not random from current image
    idx = c.files[1]
    idx = c.files[3].index(c.files[4][idx])
    c.files[0] = idx
    tkgui.redraw_image()
  else:
    keypress_navigate(c.PGDN) # next image
  tkgui.update_info()


def keypress_auto(): #--------------------------------------------------------
  c.opt_auto = not c.opt_auto
  if c.opt_auto:
    c.tkto = tkgui.after(c.opt_wait, cb_root_timer)
  else:
    if c.tkto: # cancel timer, if any
      tkgui.after_cancel(c.tkto)
      c.tkto = None
  tkgui.update_info()


def keypress_pathlock(): #----------------------------------------------------
  c.opt_lock = not c.opt_lock
  if c.opt_lock:
    # backup current files list
    c.opt_lock_files = c.files
    # get current image filename
    f = c.files[2][c.files[3+c.opt_rand*1][c.files[c.opt_rand*1]]]
    fp = "{}/".format(os.path.dirname(f))
    fn = os.path.basename(f)
    # got our filter and do random on new set
    af = filter(lambda x: x.startswith(fp), c.files[2])
    afi = range(len(af))
    c.files = [0, 0,  af, afi, random.sample(afi, len(afi))]
    # restore current image index
    fi = c.files[2].index(f)
    c.files[0] = fi
    c.files[1] = c.files[4].index(fi)
  else:
    # set current file idx and random idx to current one
    f = c.files[2][c.files[3+c.opt_rand*1][c.files[c.opt_rand*1]]]
    fi = c.opt_lock_files[2].index(f)
    # restore complete files list
    c.files = c.opt_lock_files
    c.files[0] = fi
    #c.files[1] = c.files[4].index(fi)
    c.opt_lock_files = None
  tkgui.update_info()


def keypress_interval(keycode): #---------------------------------------------
  if keycode == c.BRACKETO:
    c.opt_wait -= 500
  else:
    c.opt_wait += 500
  c.opt_wait = (1000 if c.opt_wait < 1000 else c.opt_wait)
  tkgui.update_info()


def keypress_origsize(): #----------------------------------------------------
  c.opt_orig = not c.opt_orig
  tkgui.redraw_image()
  tkgui.update_info()


# Functions ==================================================================
def parse_arguments(): #------------------------------------------------------
  """Reads the given command-line arguments"""

  parser = argparse.ArgumentParser(add_help=False,
    formatter_class=argparse.RawTextHelpFormatter,
    description="""  Python Image viEWer\n\nassigned keys:
  `        - toggle information box
  f        - toggle fullscreen
  o        - toggle original size
  p        - toggle path lock
  q        - quit application
  r        - toggle random
  s        - toggle slideshow
  [,]      - change slideshow interval
  -,=      - change zoomfactor
  h,j,k,l  - change zoom panning

  pageup,   b, backspace  - previous image
  pagedown, n, space      - next image""")

  parser.add_argument('-h', dest="help"
                     ,help="show this help message and exit"
                     ,action="store_true", default=False)

  parser.add_argument('-q', dest="quit_immediately"
                     ,help="find files and quit"
                     ,action="store_true", default=False)

  parser.add_argument('-s', dest="slideshow"
                     ,help="start fullscreen slideshow"
                     ,action="store_true", default=False)

  parser.add_argument('-n', dest="non_recursive"
      ,help="do not recurse in directories*"
                     ,action="store_true", default=False)

  parser.add_argument('-r', dest="recursive"
      ,help="recurse into directories (default)*"
                     ,action="store_true", default=False)

  parser.add_argument("remains"
                      ,nargs=argparse.REMAINDER
                      ,metavar='... path, filename or filter ')

  args = parser.parse_args()

  if args.help:
    parser.print_help()
    sys.exit(0)

  if args.slideshow:
    c.opt_slideshow = True

  if args.non_recursive:
    c.opt_recursive = False

  # set the files to find
  _f = ['.'] # defaults to current directory
  if args.remains:
    if len(args.remains) == 1 and os.path.isfile(args.remains[0]):
      # we got only one file (could be opened from filemanager)
      # let's include any other images from the directory (non-recursively)
      c.opt_recursive = False or args.recursive
      _f = args.remains[0].rsplit('/',1)[0]
      if os.path.isfile(_f):
        _f = ['.']
      else:
        # the single file is in a subfolder
        _f = [_f]
      # and we want this file to be shown first :)
      c.arg_file = os.path.abspath(args.remains[0])
    else:
      _f = set(args.remains) # uniq

  file_finder(_f)
  if args.quit_immediately:
    sys.exit(0)


def file_finder(args): #------------------------------------------------------
  if not args:
    return False
  else:
    files = []
    filters = {}

    for arg in args:
      # handle each given argument appropriate
      # assumption: it's either a path, file or filter
      if os.path.isdir(arg):
        arg = os.path.abspath(arg)
        fstr = "index : `{}'"
        s = (arg[(79-len(fstr)-1):] and '...')+arg[-(79-len(fstr)-1):]
        fstr = fstr.format(s)
        print(fstr)
        found_files = find_files(arg)
        files.extend(found_files)
      elif os.path.isfile(arg):
        arg = os.path.abspath(arg)
        if os.path.splitext(arg)[1] in c.ext:
          files.extend([arg])
      else:
        filters[arg]=0

    # apply filters in given order
    if len(filters) > 0:
      filtered = []
      for _filter in filters:
        #print "apply filter", afilter
        result = [ _file for _file in files if _file.find(_filter) > 0 ]
        filters[_filter] = len(result)
        filtered += result
      files = filtered
      filters = [ '"{}"={}'.format(_f,filters[_f]) for _f in filters]
      filters = ', '.join(filters)
      print('total : {} file{} ({})'.format(
          len(files), ('s' if len(files) != 1 else ''), filters,
        ))
    else:
      if len(args) > 1:
        print("total : {} files".format(len(files)))

    # sort by path, filename
    files = sorted(files, key=lambda f: ( f.lower().rsplit('/',1)[0],
      f.lower().rsplit('/',1)[1]))
    # create list with indexes and randomized indexes
    files_index = range(len(files))
    # c.files[0] = file index, c.files[1] = random file index
    c.files = [-1, 0,  files, files_index, random.sample(files_index, len(files))]

    if c.arg_file:
      # if the user specified a single file, set the index for it
      idx = c.files[2].index(c.arg_file)
      c.files[0] = idx-1 # will be increased later on..
      fstr = "show  : `{}'"
      s = (c.arg_file[(79-len(fstr)-1):] and '...')+c.arg_file[-(79-len(fstr)-1):]
      fstr = fstr.format(s)
      print(fstr)


def find_files(path): #-------------------------------------------------------
  sys.stdout.write('found :       ')
  matches = []
  if not os.path.exists(path):
    return matches
  matches = []

  if c.opt_recursive:
    for root, dirnames, filenames in os.walk(path):
      for filename in filenames:
        ret = find_files_proc_file(root, filename)
        if ret:
          matches.append(ret)
      sys.stdout.write('\b\b\b\b\b\b{:<6}'.format(len(matches)))
      sys.stdout.flush()
  else:
    for filename in os.listdir(path):
      ret = find_files_proc_file(path, filename)
      if ret:
        matches.append(ret)
      sys.stdout.write('\b\b\b\b\b\b{:<6}'.format(len(matches)))
      sys.stdout.flush()
  print
  return matches


def find_files_proc_file(root, filename):
  # processes a single file with respect to options (foremost c.ext)
  ret = None

  full_path = os.path.join(root, filename)
  if os.path.islink(full_path):
    # check if valid symlink
    target = os.readlink(full_path)
    if os.path.isabs(target):
      target = os.path.join(os.path.dirname(full_path),target)
    if not os.path.exists(target):
      full_path = ''
  ext = os.path.splitext(full_path)[1]
  if ext and os.path.splitext(full_path)[1] in c.ext:
    ret = full_path

  return ret


# ============================================================================
# ============================================================================
# ============================================================================
if __name__ == "__main__":

  # main configuration
  c = Config()

  # handle given arguments
  try:
    parse_arguments()
  except KeyboardInterrupt:
    sys.exit(0)

  if len(c.files[2]) == 0:
    sys.stderr.write('warn : no files found, quit\n')
    sys.exit(1)

  # create Tkinter GUI
  tkgui = PUI(Tk())
  # add "external" key handler callback
  tkgui.bind('<Key>', cb_root_keypress)
  tkgui.bind("<Button-4>", cb_root_mouse)
  tkgui.bind("<Button-5>", cb_root_mouse)

  if c.opt_slideshow:
    # use timer to let gui setup first
    # so fullscreen works
    tkgui.after(1, cb_start_slideshow)
  else:
    # simulate "first" next image
    keypress_navigate(c.PGDN)

  tkgui.mainloop()


# vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 textwidth=79
