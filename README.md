# piew: simple python image viewer

Piew is a simple Python Image viEWer, which uses TKinter to display an image on
the screen. One can pass folders, images and or simple filters as arguments to
the script.  It will sort out what type it is and create a acumulated list of
all images found in the specified location(s). Filters are very basic though.


## installation

Just copy the script `piew.py` to a directory part of the PATH environment
variable to make it execute when typed at any location.

example: `cp piew.py /usr/local/bin/piew`


## notes

By default it also tries to find image files in subdirectories from the given
positional arguments. The following exceptions occur:
  1. if only one file is given as an argument (ex. started from a file
     manager), it will not recurse into subdirectories; use -r to (re-en)force
  2. use -n to prevent "recursiveness"


## usage

```bash
usage: piew [-h] [-s] [-q] ...

  Python Image viEWer

assigned keys:
  `        - toggle information box
  f        - toggle fullscreen
  o        - toggle original size
  p        - toggle path lock
  q        - quit application
  r        - toggle random
  s        - toggle slideshow
  [,]      - change slideshow interval
  -,=      - change zoomfactor
  h,j,k,l  - change zoom panning

  pageup,   b, backspace  - previous image
  pagedown, n, space      - next image

positional arguments:
  ... path, filename or filter

optional arguments:
  -h                    show this help message and exit
  -q                    find files and quit
  -s                    start fullscreen slideshow
  -n                    do not recurse in directories*
  -r                    recurse into directories (default)*
```
